import { MainMessage, RendererMessage } from "../../common/messages";

export type Router = Omit<Electron.MessagePortMain, "postMessage" | "on"> & {
    postMessage(message: MainMessage): void;
    on(
        event: "message",
        listener: (
            ev: Omit<Electron.MessageEvent, "data"> & { data: RendererMessage },
        ) => void,
    ): Router;
};
