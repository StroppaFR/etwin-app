import Path from "path";
import url from "url";

import Electron from "electron";
import { gt as semverGt } from "semver";

import type { RendererMessage } from "../../common/messages.js";
import { BrowserViewManager } from "./BrowserViewManager.js";
import { findFlashPlugin } from "./flash.js";
import type { Router } from "./Router.js";
import { Settings } from "./Settings.js";
import { getAppReleases, getGames } from "../../common/etwinApi.js";
import { nodeFetch } from "../../common/fetch.js";
import { DEAD } from "@eternal-twin/mt-dns";
import { DnsResolver } from "./dns-resolver.js";

const DNS_RESOLVER = DnsResolver.fromRecords(DEAD);

const HOST_RULES: Map<string, string> = new Map();

for (const [domain, addr] of DNS_RESOLVER.getAll()) {
  const cleanDomain = domain.endsWith(".") ? domain.substring(0, domain.length - 1) : domain;
  HOST_RULES.set(cleanDomain, addr);
  HOST_RULES.set(`*.${cleanDomain}`, addr);
}

HOST_RULES.set("minitroopers.com", "178.32.123.64");
HOST_RULES.set("*.minitroopers.com", "178.32.123.64");
HOST_RULES.set("minitroopers.es", "178.32.123.64");
HOST_RULES.set("*.minitroopers.es", "178.32.123.64");
HOST_RULES.set("minitroopers.fr", "178.32.123.64");
HOST_RULES.set("*.minitroopers.fr", "178.32.123.64");
HOST_RULES.set("skywar.net", "178.32.123.64");
HOST_RULES.set("*.skywar.net", "178.32.123.64");


const HOST_RULES_STRING = [...HOST_RULES].map(([from, to]) => `MAP ${from} ${to}`).join(",");

Electron.app.commandLine.appendSwitch('host-rules', HOST_RULES_STRING)

const settings = new Settings();

const flash = findFlashPlugin();
if (flash) {
  Electron.app.commandLine.appendSwitch("no-sandbox");
  Electron.app.commandLine.appendSwitch("ppapi-flash-path", flash.path);
  Electron.app.commandLine.appendSwitch("ppapi-flash-version", flash.version);
} else {
  console.warn("failed to resolve Flash plugin");
}

Electron.Menu.setApplicationMenu(null);

/**
 * The Etwin app is an Electron app, thus it is divided in multiple processes.
 * For convenience, I chose to group the app pages (like the homepage) under the
 * `etwin:` protocol. They’re still technically part of the chrome, but they’re
 * shown at the same place as the “true” web pages, so they must feel the same
 * to Users.
 *
 * When a `BrowserView` loads an `etwin:` page, it should remove itself from the
 * window and let the User see the real page shown by the chrome. We do this to
 * avoid desyncs between the Main and the Renderer and to keep the browsing
 * state on the Main process.
 *
 * Alas, this doesn’t work: `BrowserView`s refuse to load unknown protocols,
 * and we can’t support custom protocols like we can with the main window.
 *
 * The solution is to use the `about:` protocol. It is supported by the
 * `BrowserView` and doesn’t render anything. But we can’t just replace the
 * protocol (`about:home` is transformed into `about:blank#blocked`)! So we use
 * the fragment to store the true page, add a translation layer, and both
 * processes are happy.
 *
 * That’s why we wrap the router. It’s not elegant but, hey, it works!
 */
function wrapRouter(router: Router): Router {
  const replaceUrl = (o: { url: string }, rx: RegExp, s: string) =>
    (o.url = o.url.replace(rx, s));
  const {on, postMessage} = router;
  router.on = (event, listener) =>
    event === "message"
      ? on.call(router, event, ev => {
        const pl = ev.data[1];
        "url" in pl && replaceUrl(pl, /^etwin:/, "about:blank#");
        return listener(ev);
      })
      : on.call(router, event, listener);
  //NOTE: don’t forget to wrap `once` if we ever use it!
  router.postMessage = message => {
    const pl = message[1];
    pl && "url" in pl && replaceUrl(pl, /^about:blank#/, "etwin:");
    postMessage.call(router, message);
  };
  return router;
}

async function queryGames(router: Router) {
  const games = await getGames(nodeFetch);
  router.postMessage(["data:games", {games}]);
  setTimeout(() => queryGames(router), 1000 * 60 * 30);
}

async function queryLastVersion(router: Router) {
  const {latest} = await getAppReleases(nodeFetch);
  console.log({lhs: latest.version, rhs: import.meta.env.PACKAGE_VERSION});
  if (semverGt(latest.version, import.meta.env.PACKAGE_VERSION)) {
    router.postMessage(["data:update", {newVersion: latest.version}]);
  }
  setTimeout(() => queryLastVersion(router), 1000 * 60 * 30);
}

function getChromeUrl(): URL {
  if (import.meta.env.DEV) {
    return new URL(`http://localhost:${process.env["PORT"] ?? 3000}`);
  } else {
    const p = Path.join(Electron.app.getAppPath(), "chrome", "index.html");
    return url.pathToFileURL(p);
  }
}

function getArgs(): string[] {
  const isBundled = !process.defaultApp;
  // Bundled apps are called with `["/path/to/etwin", ...args]`
  // Unbundled apps are called with `["/path/to/electron", "/path/to/index.js", ...args]`
  const argStart = isBundled ? 1 : 2;
  return process.argv.slice(argStart);
}

function getInitialUrl(): URL | undefined {
  const args = getArgs();
  if (args.length >= 1) {
    const url: string = args[0];
    try {
      return new URL(url);
    } catch(e) {
      console.warn(`invalid URL ${JSON.stringify(url)}: ${e}`);
    }
  }
  return undefined;
}

function createWindow() {
  const win = new Electron.BrowserWindow({
    width: settings.bounds.width,
    height: settings.bounds.height,
    x: settings.bounds.x,
    y: settings.bounds.y,
    icon: Path.join(Electron.app.getAppPath(), "icon.png"),
    webPreferences: {
      defaultEncoding: "utf8",
      contextIsolation: true,
      nodeIntegration: false, // just to be sure
      preload: Path.join(Electron.app.getAppPath(), "preload.js"),
      sandbox: true,
      plugins: true,
    },
  });

  const initiaUrl: URL | undefined = getInitialUrl();

  const chromeUrl = getChromeUrl();
  win.loadURL(chromeUrl.toString());

  if (import.meta.env.DEV) {
    win.webContents.openDevTools({mode: "detach"});
  }

  win.on("close", () => {
    settings.bounds = win.getBounds();
  });

  Electron.ipcMain.once("router", ({ports}) => {
    const router: Router = wrapRouter(ports[0]);
    router.start();
    queryGames(router);
    queryLastVersion(router);
    router.postMessage(["data:initialUrl", {initialUrl: initiaUrl === undefined ? "etwin:home" : initiaUrl.toString()}]);
    const tabManager = new BrowserViewManager(win, router);
    router.on("message", ev => {
      const [type, pl] = ev.data as RendererMessage;
      import.meta.env.DEV && console.dir({type, pl});
      switch (type) {
        case "tab:new":
          tabManager.new(pl.id);
          tabManager.goto(pl.id, pl.url);
          break;
        case "tab:show":
          tabManager.show(pl.id);
          break;
        case "tab:goto":
          tabManager.goto(pl.id, pl.url);
          break;
        case "tab:close":
          tabManager.delete(pl.id);
          break;
        case "tab:reload":
          tabManager.reload(pl.id);
          break;
        case "tab:back":
          tabManager.back(pl.id);
          break;
        case "tab:forward":
          tabManager.forward(pl.id);
          break;
        case "tab:zoomIn":
          tabManager.zoomIn(pl.id);
          break;
        case "tab:zoomOut":
          tabManager.zoomOut(pl.id);
          break;
        case "tab:zoomReset":
          tabManager.zoomReset(pl.id);
          break;
        case "tab:openDevTools":
          tabManager.openDevTools(pl.id);
          break;
      }
    });
    win.webContents.on("before-input-event", tabManager.inputListener);
  });
}

Electron.app.on("will-quit", () => {
  settings.save();
});

Electron.app.on("ready", () => {
  createWindow();
});

// fix <https://github.com/advisories/GHSA-3p22-ghq8-v749>
Electron.app.on("web-contents-created", (_, webContents) => {
  webContents.on("select-bluetooth-device", (event, _, callback) => {
    // Prevent default behavior
    event.preventDefault();
    // Cancel the request
    callback("");
  });
});
