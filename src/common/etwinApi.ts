import type { Fetch } from "./fetch";

import TempGamesJson from "../electron/lib/temp-games.json";

const HOST = "https://eternal-twin.net";
const ROOT = HOST + "/api/v1";

export type GetAppReleases = {
    latest: {
        time: string;
        version: string;
    };
};

export async function getAppReleases(fetch: Fetch) {
    const url = ROOT + "/app/releases";
    const res = await fetch(url, {
        requestOptions: { rejectUnauthorized: false },
    });
    return res.json<GetAppReleases>();
}

export type GameGroup = {
    icon?: string;
    title: string;
    link: string;
    items: GameMeta[];
};

export type GameMeta = {
    icon?: "dead" | string;
    title: string;
    link: string;
};

export async function getGames(_: Fetch): Promise<GameGroup[]> {
    //TODO: make a real http call once the endpoint is in prod
    return TempGamesJson as GameGroup[];
}
