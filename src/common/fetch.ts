import type Stream from "stream";
import type Http from "http";
import type { RequestOptions } from "https";
// we don’t want to import that in the browser
const Https = import("https").catch(() => ({} as typeof import("https")));

export class Response {
    constructor(
        public status: number,
        public headers: Http.IncomingHttpHeaders,
        private bodyStream:
            | [0, string]
            | [1, Stream.Readable]
            | [2, globalThis.Response],
    ) {}

    text(): Promise<string> {
        const [type, stream] = this.bodyStream;
        switch (type) {
            case 0:
                return Promise.resolve(stream);
            case 1:
                return new Promise(resolve => {
                    let data = "";
                    stream.on("data", chunk => (data += chunk.toString()));
                    stream.on("end", () => resolve(data));
                });
            case 2:
                return stream.text();
        }
    }

    json<T>(): Promise<T> {
        return this.text().then(JSON.parse);
    }
}

export type Fetch = (
    url: string,
    o?: {
        body?: string;
        method?: string;
        headers?: Record<string, string>;
        requestOptions?: RequestOptions;
        requestInit?: RequestInit;
    },
) => Promise<Response>;

export const nodeFetch: Fetch = (url, o = {}) =>
    Https.then(
        Https =>
            new Promise(resolve => {
                const req = Https.request(url, {
                    headers: o.headers,
                    method: o.method,
                    ...o.requestOptions,
                });
                req.on("response", res =>
                    resolve(
                        new Response(res.statusCode!, res.headers, [1, res]),
                    ),
                );
                req.on("error", err =>
                    resolve(new Response(0, {}, [0, err.message])),
                );
                o.body && req.write(o.body);
                req.end();
            }),
    );

export const browserFetch: Fetch = (url, o = {}) =>
    fetch(url, {
        method: o.method,
        headers: o.headers,
        body: o.body,
        ...o.requestInit,
    })
        .then(
            res =>
                new Response(
                    res.status,
                    Object.fromEntries(res.headers.entries()),
                    [2, res],
                ),
        )
        .catch(err => new Response(0, {}, [0, err.message]));
