import "./reset.css";
import "./style.css";
import "./etwin.css";
import "./lib/shims";

import m from "mithril";

import { EtwinUrl } from "../common/constants";
import type { GameGroup } from "../common/etwinApi";
import { BrowserBar } from "./features/browser-bar";
import { BrowserTabList } from "./features/browser-tab-list";
import { EtwinLink } from "./features/etwin-link";
import { EtwinHome } from "./pages/etwin-home";

interface State {
    tabs: BrowserTabList.State;
    bar: BrowserBar.State;
    games: undefined | GameGroup[];
    newVersion: null | string;
    initialUrl: null | string;
}

const state: State = {
    tabs: BrowserTabList.makeState(),
    bar: BrowserBar.makeState(),
    games: undefined,
    newVersion: null,
    initialUrl: null,
};
const actions = {
    tabs: (() => {
        const actions = BrowserTabList.makeActions(state.tabs);
        const { closeTab, newTab, setActiveTabId } = actions;
        actions.closeTab = id => {
            if (closeTab(id)) {
                window.etwin.send(["tab:close", { id }]);
                return true;
            }
            return false;
        };
        actions.newTab = (o = {}) => {
            const tab = newTab(o);
            const { id, url } = tab[0];
            window.etwin.send(["tab:new", { id, url: url.href }]);
            o.type !== "background" && window.etwin.send(["tab:show", { id }]);
            return tab;
        };
        actions.setActiveTabId = id => {
            setActiveTabId(id);
            window.etwin.send(["tab:show", { id }]);
        };
        return actions;
    })(),
    bar: BrowserBar.makeActions(state.bar),
    setInitialUrl(v: string) {
        const isFirst = state.initialUrl === null;
        state.initialUrl = v;
        if (isFirst && v !== state.tabs.activeTab[0].url.toString()) {
            window.etwin.send([
                "tab:goto",
                {
                    id: state.tabs.activeTab[0].id,
                    url: v,
                },
            ]);
        }
    },
    setNewVersion(v: null | string) {
        state.newVersion = v;
    },
};

window.etwin.send([
    "tab:new",
    {
        id: state.tabs.activeTab[0].id,
        url: state.tabs.activeTab[0].url.href,
    },
]);
window.etwin.send(["tab:show", { id: state.tabs.activeTab[0].id }]);

window.document.title = `Eternaltwin ${window.etwin.versions.etwin ?? "?"}`

window.etwin.onmessage(([type, pl]) => {
    import.meta.env.DEV && console.debug(type, pl);
    switch (type) {
        case "tab:favicons":
            actions.tabs.iterTab(pl.id, ([, a]) => a.setFavicon(pl.favicons));
            break;
        case "tab:loading":
            actions.tabs.iterTab(pl.id, ([, a]) => a.setLoading(true));
            break;
        case "tab:stoploading":
            actions.tabs.iterTab(pl.id, ([, a]) => a.setLoading(false));
            break;
        case "tab:url":
            actions.tabs.iterTab(pl.id, ([, a]) => a.setUrl(new URL(pl.url)));
            break;
        case "tab:title":
            actions.tabs.iterTab(pl.id, ([, a]) => a.setTitle(pl.title));
            break;
        case "tab:history":
            actions.tabs.iterTab(pl.id, ([, a]) =>
                a.setHistory(pl.canGoBack, pl.canGoForward),
            );
            break;
        case "keybind:newtab":
            if (pl) {
                actions.tabs.newTab({
                    type: pl.background ? "background" : undefined,
                    url: pl.url,
                });
            } else {
                actions.tabs.newTab();
            }
            break;
        case "keybind:closeactivetab":
            actions.tabs.closeTab(state.tabs.activeTab[0].id);
            break;
        case "keybind:tableft":
            actions.tabs.setActiveOffset(-1);
            break;
        case "keybind:tabright":
            actions.tabs.setActiveOffset(1);
            break;
        case "keybind:focusinput":
            state.bar.inputRef?.select();
            break;
        case "data:games":
            state.games = pl.games;
            break;
        case "data:initialUrl":
            actions.setInitialUrl(pl.initialUrl);
            break;
        case "data:update":
            actions.setNewVersion(pl.newVersion);
            break;
    }
    m.redraw();
});

if (window.etwin.env === "electron") {
    EtwinLink.onclick = (url, newTab) => {
        if (newTab) {
            actions.tabs.newTab({ type: "background", url });
        } else {
            window.etwin.send([
                "tab:goto",
                { id: state.tabs.activeTab[0].id, url },
            ]);
        }
    };
}

const $Root = `.
    .fs-1/2
`;

const $Nav = `nav.
    .absolute.top-0.right-0.left-0
    .fg0
    .user-select-none
`;

const $ScrollContainer = `.
    .flex.column
    .absolute.top-5/2.right-0.bottom-0.left-0
    .overflow-auto
`;

m.mount(document.body, {
    view: () => {
        const id = state.tabs.activeTab[0].id;
        return m($Root, [
            m($Nav, [
                m(BrowserTabList, { state: state.tabs, actions: actions.tabs }),
                m(BrowserBar, {
                    state: state.bar,
                    actions: actions.bar,
                    activeTab: state.tabs.activeTab[0],
                    handleBack: () => window.etwin.send(["tab:back", { id }]),
                    handleForward: () =>
                        window.etwin.send(["tab:forward", { id }]),
                    handleReload: () =>
                        window.etwin.send(["tab:reload", { id }]),
                    handleUrl: url => {
                        state.tabs.activeTab[1].setUrl(url);
                        window.etwin.send(["tab:goto", { id, url: url.href }]);
                    },
                    handleHome: () => {
                        state.tabs.activeTab[1].setUrl(EtwinUrl.Home);
                        window.etwin.send([
                            "tab:goto",
                            { id, url: EtwinUrl.Home.href },
                        ]);
                    },
                    handleDevTools: () => {
                        window.etwin.send([
                            "tab:openDevTools",
                            { id },
                        ]);
                    },
                    handleZoomIn: () => {
                        window.etwin.send([
                            "tab:zoomIn",
                            { id },
                        ]);
                    },
                    handleZoomOut: () => {
                        window.etwin.send([
                            "tab:zoomOut",
                            { id },
                        ]);
                    },
                }),
            ]),
            m($ScrollContainer, [
                //TODO: write a pseudo-router if we have more than one page
                state.tabs.activeTab[0].url.protocol === "etwin:" &&
                    m(EtwinHome, {
                        games: state.games,
                        versions: window.etwin.versions,
                        newVersion: state.newVersion,
                    }),
            ]),
        ]);
    },
});
