import cx from "classnames";
import m from "mithril";
import { nanoid } from "nanoid/non-secure";

import SpinnerSvg from "../assets/spinner.svg?url";
import IconClear from "../assets/icon-clear.svg";
import FaviconEtwinPng from "../assets/favicon-etwin.png?url";
import { UiButton } from "./ui-button";

export namespace BrowserTab {
    export type State = {
        id: string;
        url: URL;
        title: string;
        /** etwin | favicon */ favicon: "" | string;
        loading: boolean;
        back: boolean;
        forward: boolean;
    };
    export type Actions = {
        setUrl(url: URL): void;
        setTitle(title: string): void;
        setFavicon(srcs: string[]): void;
        setLoading(value: boolean): void;
        setHistory(back: boolean, forward: boolean): void;
    };
}

const $BrowserTab = `.
    .relative
    .flex.flex-grow.items-center.gap-1/6
    .border-box
    .minw-0.maxw-8
    .h-1
    .b.b--bottom.bw-1px.bc-bg1.br-1/5
    .px-1/5
    .$group
`;

const activeLabelClass = "fg0 fw-bold fadeout-right-3/5";
const inactiveLabelClass = `
    fg1 $group-hover:fg0
    fadeout-right $group-hover:fadeout-right-3/5
`;

export const BrowserTab = (): m.Component<{
    state: BrowserTab.State;
    active: boolean;
    handleClick: (self: string) => void;
    handleClose: (self: string) => void;
}> => ({
    view: ({ attrs }) =>
        m(
            $BrowserTab,
            {
                class: attrs.active ? "bg0" : undefined,
                onclick: () => attrs.handleClick(attrs.state.id),
                onauxclick: (ev: MouseEvent) =>
                    ev.button === 1 && attrs.handleClose(attrs.state.id),
                title: attrs.state.title,
            },
            m("img.crisp[width=16][height=16]", {
                srcset: attrs.state.loading ? SpinnerSvg : attrs.state.favicon,
                src: FaviconEtwinPng,
            }),
            m(
                "p.flex-grow.nowrap.overflow-hidden",
                { class: attrs.active ? activeLabelClass : inactiveLabelClass },
                attrs.state.title,
            ),
            m(UiButton, {
                icon: IconClear,
                onclick: ev => {
                    ev.stopImmediatePropagation();
                    attrs.handleClose(attrs.state.id);
                },
                size: "small",
                class: cx("absolute right-1/10 no-shrink", {
                    "hidden $group-hover:visible": !attrs.active,
                }),
            }),
        ),
});

BrowserTab.makeState = (): BrowserTab.State => ({
    id: nanoid(8),
    url: new URL("etwin:home"),
    title: "New Tab",
    favicon: "",
    loading: false,
    back: false,
    forward: false,
});

BrowserTab.makeActions = (state: BrowserTab.State): BrowserTab.Actions => ({
    setUrl: url => (state.url = url),
    setTitle: title => (state.title = title),
    setFavicon: srcs => (state.favicon = srcs.join(" , ")),
    setLoading: value => (state.loading = value),
    setHistory: (back, forward) => {
        state.back = back;
        state.forward = forward;
    },
});
