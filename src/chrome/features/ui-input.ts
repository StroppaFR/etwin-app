import m from "mithril";

const $Input = `input.
    .flex-grow
    .border-box
    .h-1
    .px-1/5.focus:px-1/10+1px
    .b.bc-fg2.bw-1px.br-1/5
    .hover:bg1.focus:bw-1/10.focus:bg-white
    .trans-bg-1/2
    .cursor-auto
    .bg1
    .fg1
`;

export const UiInput: m.Component<{ [_: string]: unknown }> = {
    view: ({ attrs }) => m($Input, attrs),
};
