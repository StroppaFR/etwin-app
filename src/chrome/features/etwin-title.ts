import m from "mithril";

type Level = 1 | 2 | 3 | 4 | 5 | 6;

const tag: Record<Level, string> = {
    1: `h1.
        .fs-3/2.fw-300.italic.uppercase
        .etwin-fg-red
    `,
    2: `h2.
        .fs-1.fw-bold
        .twin-fg-red
    `,
    3: "h3",
    4: "h4",
    5: "h5",
    6: "h6",
};

export const EtwinTitle = (): m.Component<{
    level: Level;
}> => ({
    view: ({ attrs, children }) => m(tag[attrs.level], children),
});
