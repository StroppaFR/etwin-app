# 0.6.6 (2023-02-10)

- **[Fix]** Fix Skywar domain resolution.

# 0.6.5 (2023-01-01)

- **[Fix]** Embed game icons in the app, fixes rendering issues when downloads are blocked by an eager antivirus.

# 0.6.4 (2022-11-12)

- **[Fix]** Fix Minitroopers domain resolution.

# 0.6.3 (2022-08-29)

- **[Feature]** Add missing Eternaltwin games: La Brute, ePopotamo, Eternal Kingdom, Eternal DinoRPG, eMush.

# 0.6.2 (2022-08-26)

- **[Fix]** Restore zoom controls.
- **[Fix]** Compute Eternaltwin version in the title bar dynamically.

# 0.6.1 (2022-08-26)

- **[Internal]** Run MacOS build in CI.

# 0.6.0 (2022-08-25)

- **[Feature]** Support passing an initial URL.
- **[Fix]** Update Neoparc link.
- **[Fix]** Update to Electron 11.
- **[Fix]** Refactor chrome implementation.

# 0.5.6 (2021-12-18)

- **[Feature]** Generate `.zip` builds for Windows.
- **[Feature]** Generate `.zip` builds for Windows.
- **[Fix]** Update MyHordes link.
- **[Fix]** Update DNS resolver: Fix support for Hammerfest (EN).
- **[Fix]** Update dependencies.

# 0.5.5 (2021-10-07)

- **[Fix]** Update DNS resolver: Fix support for Twinoid, DinoRPG (DE) and Mush (ES).

# 0.5.4 (2021-10-06)

- **[Fix]** Fix expired domain resolution.
- **[Fix]** Fix update check.
- **[Fix]** Update dependencies.

# 0.5.3 (2021-06-29)

- **[Fix]** Add Popotamo to the Flash allowlist.
- **[Fix]** Update dependencies.

# 0.5.2 (2021-01-31)

- **[Fix]** Add `minitroopers.es` to the website list.

# 0.5.1 (2021-01-24)

- **[Feature]** Provide assisted installer for Windows (it enables users to pick the installation directory).
- **[Fix]** Fix Flash support for `dieverdammten.de`.
- **[Fix]** Fix Snake links in the homepage.
- **[Fix]** Restore Popotamo link in the homepage.

# 0.5.0 (2021-01-21)

- **[Feature]** Publish Windows and Mac installers
- **[Feature]** Add 32-bit support for Windows
- **[Feature]** Add notification once an update is available
- **[Fix]** Add more websites to the allowlist

# 0.4.3

- **[Feature]** Add zoom controls
- **[Fix]** Refactor chrome and built-in pages: use Typescript and Sass, build them with Webpack.
