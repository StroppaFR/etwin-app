import { defineConfig } from "vite";
import { mithrilSvgPlugin } from "./vite-plugin-mithril-svg";

export default defineConfig({
    root: "src/chrome/",
    base: "./",
    plugins: [mithrilSvgPlugin()],
    define: {
        "import.meta.env.PACKAGE_VERSION": JSON.stringify(
            process.env.npm_package_version,
        ),
    },
    build: {
        // `version.swf` has a size of 158 bytes. The config below is a workaround to prevent it from being inlined.
        assetsInlineLimit: 128,
        target: "chrome87",
    },
    server: {
        port: 3000,
        strictPort: true,
    }
});
