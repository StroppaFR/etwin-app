# TODO
- injecter un truc dans window pour dire que ça tourne sur etwin
- faire un srcset custom pour les favicons
- déplacer un onglet
- bouton pour couper le son d’un onglet
- virer le chrome natif
- ouvrir un .swf directement
- page settings
  * éditer la liste des sites où flash est autorisé à s’exécuter
- hmr dans electron en gardant l’ipc

# DONE
## User
- nouveau design
- pages internes dans `etwin:`
- se souvenir de la géométrie de la fenêtre
- fix du bug de focus de bya
## Technique
- Fix d’un leak mémoire <https://github.com/electron/electron/issues/17911>
- intégration des pages internes dans le chrome
- Utilisation de `MessageChannel` pour communiquer entre les process
- `BrowserView` au lieu de `<webview>`
- Electron 4.x => 11.5
- Remplacement de Webpack par Vite
- Réécriture des scripts de build en TypeScript avec esbuild

# NOTES
- `minimist` n’est pas utilisé directement, c’est juste pour éviter une faille critique
