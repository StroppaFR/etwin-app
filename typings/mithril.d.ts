import "mithril";

declare module "mithril" {
    export type Event = { redraw: boolean };
}
