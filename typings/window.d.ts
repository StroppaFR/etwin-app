import type { PreloadData } from "../src/common/PreloadData";

declare global {
    interface Window {
        etwin: PreloadData;
    }
}
